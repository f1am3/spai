#!/usr/bin/env python
from flask import Flask, jsonify, request

app = Flask(__name__)

logger = app.logger
logger.setLevel('INFO')

@app.route('/<path:subpath>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def handler(subpath):
    logger.info('Got {} request to {} with {} byte payload'.format(
        request.method,
        subpath,
        request.content_length,
    ))
    return jsonify(ok=True)


if __name__ == "__main__":
    app.run()
